const std = @import("std");
const expect = std.testing.expect;

const Rational = struct { num: i32, den: i32 };

fn prod(a: Rational, b: Rational) Rational {
    return Rational{ .num = a.num * b.num, .den = a.den * b.den };
}

fn add(a: Rational, b: Rational) Rational {
    return Rational{ .den = a.den * b.den, .num = a.num * b.den + b.num * a.den };
}

fn gcd(a: i32, b: i32) i32 {
    if (b == 0) return a;
    return gcd(b, @rem(a, b));
}

test "gcd basic test" {
    try expect(gcd(10, 8) == 2);
    try expect(gcd(8, 10) == 2);
}

fn simplify(f: Rational) Rational {
    const d = gcd(f.num, f.den);
    return Rational{ .num = @divFloor(f.num, d), .den = @divFloor(f.den, d) };
}

test "add" {
    const x = Rational{ .num = 1, .den = 2 };
    const y = simplify(add(x, prod(x, x)));
    try expect(y.num == 3 and y.den == 4);
}

pub fn main() void {
    const x = Rational{ .num = 1, .den = 2 };
    const y = add(x, prod(x, x));
    std.debug.print("Product is {}/{}\n", .{ y.num, y.den });
}
